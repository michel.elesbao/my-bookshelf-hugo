---
title: "Rapido E Devagar"
date: 2021-10-24T15:27:10-03:00
draft: false
description: "É um best-seller publicado em 2011 pelo, laureado em Economia, Daniel Kahneman. "
---

# Rápido e devagar

Daniel Kahneman mostra a capacidade do pensamento rápido, sua influência persuasiva em nossas decisões e até onde podemos ou não confiar nele. O entendimento do funcionamento dessas duas formas de pensar pode ajudar em nossas decisões pessoais e profissionais.[5]

O livro de Kahneman explica como o funcionamento do cérebro acontece separado em 2 sistemas. E nisso ele já nos dá uma importante lição sobre o poder de histórias para explicar conceitos complexos. Entendendo a história dos 2 sistemas proposta no livro Rápido e Devagar, é possível entender, por exemplo, como funciona a compra por impulso.

## Sobre o autor

Daniel Kahneman (Tel Aviv, 5 de março de 1934) é um teórico da economia comportamental, a qual combina a economia com a ciência cognitiva para explicar o comportamento aparentemente irracional da gestão do risco pelos seres humanos.[1]

É conhecido por sua colaboração com Amos Tversky e outros, estabelecendo uma base cognitiva para os erros humanos comuns, usando a heurística e desenvolvendo a "prospect theory".

Kahneman viveu sua infância em Paris, França e regressou para Israel em 1946. Obteve licenciatura (B.Sc.) em matemática e psicologia na Universidade Hebraica de Jerusalém em 1954, após o qual serviu nas forças de defesa israelitas, principalmente no seu departamento de psicologia. Em 1958 foi para os Estados Unidos, obtendo um doutorado em psicologia pela Universidade da Califórnia em Berkeley em 1961.

Atualmente é professor da Universidade de Princeton e um membro (fellow) da Universidade Hebraica. Foi laureado em 2002 com o Prémio do Banco da Suécia em Ciências Económicas em memória de Alfred Nobel (designado por vezes como o Prémio Nobel da Economia), apesar de ser um psicólogo, muitas vezes é considerado um economista (o que não deixa de ser válido devido seus trabalhos em relação à economia). De facto, Kahneman nunca fez qualquer cadeira em economia, afirmando que aquilo que sabe sobre o assunto foi o que ele e Tversky aprenderam com colaboradores como Richard Thaler e Jack Knetsch
