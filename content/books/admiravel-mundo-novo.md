---
title: "Admiravel Mundo Novo"
date: 2021-10-24T15:26:04-03:00
draft: false
---

# Enredo

O personagem Bernard Marx sente-se insatisfeito com o mundo onde vive, em parte porque é fisicamente diferente dos integrantes da sua casta. Num reduto onde vivem pessoas dentro dos moldes do passado uma espécie de "reserva histórica" - semelhante às atuais reservas indígenas - onde preservam-se os costumes "selvagens" do passado (que corresponde à época em que o livro foi escrito), Bernard encontra uma mulher oriunda da civilização, Linda, e o filho dela, John. Bernard vê uma possibilidade de conquista de respeito social pela apresentação de John como um exemplar dos selvagens à sociedade civilizada.

Para a sociedade civilizada, ter um filho era um ato obsceno e impensável, ter uma crença religiosa era um ato de ignorância e de desrespeito à sociedade. Linda, quando chegada à civilização foi rejeitada pela sociedade.

## Personagens

Thomas "Tomakin", Alfa, Diretor de Incubação e Condicionamento (D.I.C.) de Londres.
Henry Foster, Alfa, Administrador de Incubação e atual companheiro de Lenina.
Lenina Crowne, Beta-Mais, Vacinadora no Centro Incubação e Condicionamento. Ela usa verde, porém sempre diz que está feliz por não ser uma Gama. (Betas usam Amora, Gamas usam Verde). Amada por John, o Selvagem, e Bernard Marx.
Polly Trotsky, (Casta indefinida), uma menina que brinca de jogos eróticos no jardim do Centro Incubação e Condicionamento.
Mustapha Mond Alfa-Mais-Mais, Administrador Residente da Europa Ocidental. Um dos dez Administradores Mundiais (os outros nove, presumidamente, devem atuar em diferentes localidades do mundo.)
Diretor-Adjunto de Predestinação, (Casta indefinida), amigo de Henry Foster. Conversa com Henry sobre ter relações com Lenina.
Bernard Marx, Alfa-Mais, Psicólogo (especializado em hipnopedia).
Fanny Crowne, Beta, embriologista.
George Edzel, (Casta indefinida), amigo de Lenina, viajou com ela para o polo Norte.
Benito Hoover, Alfa, amigo de Lenina. Convidou-a para viajar para o polo Norte com ele (mas ela preferiu o convite de Bernard para o Novo México). Desafeto de Bernard.
Helmholtz Watson, Alfa-Mais, Professor do Colégio de Engenharia Emocional (Seção de Redação), melhor amigo e confidente de Bernard Marx e John, O Selvagem.
Miss Keate, Diretora do Colégio Eton.
Primo Mellon, um repórter do jornal para castas superiores Rádio Horário que tenta entrevistar John, O Selvagem.
Darwin Bonaparte, um paparazzo que traz a atenção do mundo para o eremita John.

## Enredo

Gandalf envolve Bilbo em uma festa para Thorin e seu grupo de anões, que cantam sobre recuperar a Montanha Solitária e seu vasto tesouro do dragão Smaug.[11] Quando a música termina, Gandalf revela um mapa que mostra uma porta secreta na montanha e propõe que um estupefato Bilbo sirva como "ladrão" da expedição.[12] Os anões ridicularizam tal ideia, mas o hobbit, indignado, junta-se a eles mesmo sem querer.[13]

O grupo viaja rumo às terras selvagens,[14] onde Bilbo e Gandalf salvam a companhia de um grupo de trolls[15]. Este último os leva à Rivendell,[16] onde Elrond revela os segredos do mapa que Thorin possui para a entrada secreta de Erebor.[17] Passando por cima das Montanhas Sombrias, eles são capturados por goblins e conduzidos ao subterrâneo profundo.[18] Embora Gandalf consiga resgatá-los, Bilbo acaba separado dos demais no momento da fuga.[19] Perdido nos túneis dos goblins, ele se depara com um misterioso anel e, em seguida, encontra Gollum, que o envolve em um jogo de charadas.[20] Como recompensa por resolver todos os enigmas propostos, Gollum lhe mostraria o caminho para fora dos túneis; mas, se Bilbo não conseguir decifrá-los, sua vida se perderá.[21] Com a ajuda do anel — que lhe confere invisibilidade —, Bilbo escapa e reencontra os anões, melhorando sua reputação junto a eles.[22] Os orcs e os wargs ainda os perseguem, mas o grupo é salvo por águias antes de descansar na casa de Beorn.[23]
