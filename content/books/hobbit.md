---
title: "Hobbit"
date: 2021-09-18T23:28:40-03:00
draft: false
description: "O cuidado em identificar pontos críticos no início da atividade geral de formação de atitudes ainda não demonstrou convincentemente que vai participar na mudança do levantamento das variáveis envolvidas."
---

# O hobbit

The Hobbit, or There and Back Again (publicado em Portugal como O Gnomo ou O Hobbit e, no Brasil, como O Hobbit ou Lá e de Volta Outra Vez ou simplesmente O Hobbit) é um livro infantojuvenil de alta fantasia escrito pelo filólogo e professor britânico J. R. R. Tolkien. Publicado originalmente em 21 de setembro de 1937, foi aclamado pela crítica, sendo nomeado à Medalha Carnegie e recebendo um prêmio do jornal norte-americano New York Herald Tribune de melhor ficção juvenil. O romance se mantém popular com o passar dos anos e é reconhecido como um clássico da literatura infantil, tendo vendido mais de 190 milhões de cópias.

## Personagens

Bilbo Bolseiro, o protagonista, é um hobbit respeitável, avesso a perigos e tudo o mais que interfira na vida mansa e farta de um hobbit que se preza.[4][5] Muitas vezes durante a aventura, em especial nos momentos mais perigosos, Bilbo lamenta o conforto e a boa comida que deixou para trás. Essa natureza pacífica e levemente indolente fazem com que, até conseguir o anel mágico que o torna invisível, Bilbo seja tanto um peso quanto uma ajuda na busca dos anões, colocando seus companheiros em apuros para imediatamente tirá-los com a astúcia e a percepção dos detalhes também típicas dos hobbits. Gandalf é o feiticeiro andarilho[6] que introduz Bilbo à companhia de treze anões cuja missão é narrada pelo livro. Durante a jornada, o feiticeiro desaparece em missões secundárias vagamente inusitadas, reaparecendo somente em momentos-chave da história. Thorin Escudo de Carvalho, o orgulhoso e pomposo[7][8] chefe do grupo de anões e herdeiro do destruído reino sob a Montanha Solitária, comete muitos erros em sua liderança, tendo sempre Gandalf e Bilbo para tirá-lo dos perigos; contudo, ele sempre se mostra um guerreiro honrado e preocupado com seu povo. Smaug é um dragão que há muito tempo saqueou Erebor, o reino criado por Thror, pai de Thrain e avô de Thorin, e que desde então dorme sobre o vasto tesouro amealhado pelos anões.

## Enredo

Gandalf envolve Bilbo em uma festa para Thorin e seu grupo de anões, que cantam sobre recuperar a Montanha Solitária e seu vasto tesouro do dragão Smaug.[11] Quando a música termina, Gandalf revela um mapa que mostra uma porta secreta na montanha e propõe que um estupefato Bilbo sirva como "ladrão" da expedição.[12] Os anões ridicularizam tal ideia, mas o hobbit, indignado, junta-se a eles mesmo sem querer.[13]

O grupo viaja rumo às terras selvagens,[14] onde Bilbo e Gandalf salvam a companhia de um grupo de trolls[15]. Este último os leva à Rivendell,[16] onde Elrond revela os segredos do mapa que Thorin possui para a entrada secreta de Erebor.[17] Passando por cima das Montanhas Sombrias, eles são capturados por goblins e conduzidos ao subterrâneo profundo.[18] Embora Gandalf consiga resgatá-los, Bilbo acaba separado dos demais no momento da fuga.[19] Perdido nos túneis dos goblins, ele se depara com um misterioso anel e, em seguida, encontra Gollum, que o envolve em um jogo de charadas.[20] Como recompensa por resolver todos os enigmas propostos, Gollum lhe mostraria o caminho para fora dos túneis; mas, se Bilbo não conseguir decifrá-los, sua vida se perderá.[21] Com a ajuda do anel — que lhe confere invisibilidade —, Bilbo escapa e reencontra os anões, melhorando sua reputação junto a eles.[22] Os orcs e os wargs ainda os perseguem, mas o grupo é salvo por águias antes de descansar na casa de Beorn.[23]
