---
title: "O senhor dos anéis - A sociedade do anéis"
date: 2021-09-18T23:27:57-03:00
draft: false
description: "O Senhor dos Anéis (The Lord of the Rings, no original) é um livro de alta fantasia, escrito pelo escritor britânico J. R. R. Tolkien. Escrita entre 1937 e 1949, com muitas partes criadas durante a Segunda Guerra Mundial, a saga é uma continuação de O Hobbit (1937)."
---

# O senhor dos anéis

Frodo Bolseiro é um hobbit do Condado, que recebe de seu tio Bilbo um anel de rara beleza. Esse anel tem uma longa história: foi roubado de uma criatura chamada Gollum (como relatado no livro O Hobbit), e desde então ele tem sido guardado por Bilbo.

O Mago Gandalf, um velho amigo de Bilbo, percebe o poder que aquele anel possui, não sendo um anel comum, mas sim o Um Anel, artefato mágico forjado por Sauron, o Senhor Sombrio, e que fora perdido numa batalha muito tempo antes. Se recuperado, o Um Anel permitiria a Sauron o domínio definitivo sobre toda a Terra-média. O Um Anel, ou Anel do Poder, dera longevidade fora do comum a seu antigo dono, Bilbo, e possuía consciência, uma vontade própria que o conduzia sempre na direção do seu criador e senhor. Gandalf aconselha Bilbo a deixar o Condado, planejado para ocorrer até a festa de aniversário daquele ano. Gandalf parte, para resolver alguns assuntos, mas combinando que voltaria para acompanhar Frodo, porém, não manda notícias durante vários meses. Chegando a data prevista, Bilbo decide deixar o Condado e deixa tudo para Frodo. Depois de um tempo, Gandalf retorna e convence Frodo a partir para destruir o anel, após vender Bolsão, Frodo leva consigo seus amigos Sam, Merry e Pippin para sua aventura.

## Enredo

Os hobbits resolvem pegar um atalho que passa através da Floresta Velha, lar de árvores que se comunicam entre si. Dentro da Floresta, os hobbits são salvos de uma árvore violenta por um estranho ser que adora cantar: o enigmático Tom Bombadil, um dos maiores mistérios de Tolkien.

Passando por outros perigos, os hobbits chegam a Bree, uma vila habitada por Homens e hobbits, perto da fronteira do Condado, e lá aceitam a ajuda de um Guardião chamado Passolargo, amigo de Gandalf, que os guia até Valfenda, um reino ainda habitado por elfos, seres imortais, detentores de grande poder, beleza e sabedoria. Mas o caminho ainda é perigoso: o grupo é emboscado no Topo do Vento e Frodo acaba apunhalado por um Nazgûl, Espectro do Anel. Passolargo consegue repelir a ofensiva do Inimigo e foge com Frodo, que está gravemente ferido, e os outros hobbits. Quando estão a ser novamente alcançados pelos Espectros do Anel, o elfo Glorfindel encontra-os e os conduz com segurança até Valfenda. Os Nazgûl tentam detê-los mas são varridos pela inundação súbita do rio Bruinem.

Já curado, Frodo descobre as maravilhas de Valfenda e lá é realizado um conselho liderado por Elrond, o meio-elfo mestre de Valfenda e pai de Arwen, a amada de Passolargo, cujo verdadeiro nome é Aragorn, que se revela descendente de Isildur e herdeiro do Trono de Gondor.

No Conselho de Elrond, são expostos os problemas relacionados ao Um Anel. Boromir, filho do regente de Gondor, sugere usar o Anel do Poder contra Sauron. Elrond e Gandalf rejeitam a ideia imediatamente e explicam os vários motivos pelos quais não podem usar o poder do Anel contra o Senhor Sombrio: Sauron é o único e verdadeiro Senhor do Anel, pois o forjou, sendo portanto totalmente maligno, além disso, seu poder é grande demais para ser controlado por mortais comuns e mesmo os poderosos entre os povos livres da Terra-Média, como os imortais elfos (Elrond) e os magos (Gandalf), temem inclusive tocá-lo.

O poder quase absoluto do anel corrompe o carácter e deforma a personalidade daquele que se atreve a empunhá-lo, ainda que movido por boas intenções. Quem quer que tente derrotar Sauron utilizando o anel, acabará tornando-se o próximo Senhor Sombrio.

Dada a impossibilidade de utilizar o Um Anel como arma de guerra, é imposta a tarefa de levá-lo até o Monte da Perdição, um vulcão localizado no centro de Mordor, a Terra Negra do Inimigo, onde o anel fora forjado e também o único lugar onde poderia ser destruído.

Para essa missão, de sucesso improvável, é formada a Sociedade do Anel, composta por nove companheiros: quatro hobbits (Frodo, Sam, Merry e Pippin), dois humanos (Aragorn e Boromir), um elfo (Legolas), um anão (Gimli) e um mago (Gandalf). Frodo seria o "Portador do Anel", aquele que deveria lançar o Anel nos fogos de Orodruin.

A Sociedade do Anel parte em direção ao sul. Cientes que essa rota está sendo vigiada pelo Inimigo, o grupo faz um desvio para Leste através das montanhas Hithaeglir, mas são obrigados a voltar por causa da neve e do frio. Um caminho alternativo leva-os até a temida Moria, reino subterrâneo dos anãos, onde são emboscados por um Balrog, um demônio do mundo antigo, Gandalf luta com ele e morre. Os outros companheiros escapam e chegam em segurança a Lothlórien, reino da rainha élfica Galadriel, temida por seu poder mas dotada de rara beleza e sabedoria. Nesse reino encantado, onde o tempo parece não passar, os viajantes recebem auxílio e conselhos. Após algumas semanas de descanso, a Sociedade do Anel agora liderada por Aragorn, parte de Lothlorien em direção ao Sul, navegando pelo grande rio Anduin em canoas construídas pelos elfos da Floresta Dourada. Quando param para descansar próximo às cataratas de Rauros, Boromir tem uma discussão com Frodo, e tenta roubar-lhe o Anel do Poder. Frodo foge e decide ir sozinho para Mordor, mas acaba levando Sam. Quando os outros membros da Sociedade do Anel vão em busca de Frodo, são atacados por Uruk-hai (sub-espécie de Orques, mais alta e forte) enviados por Saruman, um mago renegado que se aliou a Sauron, mas que também ambiciona o Anel do Poder.
